\section{Simplifications}
\subsection{Phenomenological Laws}
For the nonadvective (dispersive) fluxes of mass $J_i^{\text{dis}}$ and momentum $\sigma$ phenomenological laws are available. Dispersive mass flux is expressed as an Fickian type law
\begin{equation}
J_i^{\text{dis}} = - \rho_R \mathbf{D}_h \nabla \omega_i \label{equation_dispersive_flux}
\end{equation}
with $\mathbf{D}_h$ $\left[\frac{m^2}{s}\right]$ denoting the hydrodynamic dispersion tensor, which is assumed to be independent of the concentration and its gradient. According to the \textit{Bear-Scheidegger} dispersion relationship for isotropic porous media it reads
\begin{equation}
\mathbf{D}_h = \phi \left( \mathbf{D}_D + \beta_T \|\velocity\| \mathbf{I} + \left(\beta_L - \beta_T\right)\frac{\velocity \velocity^T}{\|\velocity\|} \right)
\end{equation} 
with molecular diffusion coefficient $\mathbf{D}_D$, longitudinal $\beta_L$ and transversal $\beta_T$ dispersivity coefficients and the identity tensor $\mathbf{I}$. \\
The stress tensor for the fluid phase can be expressed assuming the validity of \textit{Newton's viscosity law} , with hydrostatic pressure $p$ and $\sigma^l$ denoting the deviatoric stress tensor depending on the dynamic viscosity $\mu$ $\left[\frac{kg}{ms}\right]$ and the velocity gradient $\nabla v$,
\begin{equation}
\sigma = -\phi\left( p \textbf{I} + \sigma^l(\mu, \nabla \velocity) \right).
\end{equation}

\subsection{Density Differential}
The density function $\rho_R(p, \omega_C)$ is regarded as a function of pressure and component concentration. Hence, its total differential reads
\begin{equation}
\mathrm{d}\rho_R = \frac{\partial \rho_R}{\partial p} \mathrm{d} p + \frac{\partial \rho_R}{\partial \omega_C} \mathrm{d}\omega_C. \label{equation_density_functional}
\end{equation}
From the relation above the density function can be obtained immediately by integration.
\subsection{Constitutive Equations}
The interfacial drag term $F$ of momentum exchange can be developed up to the 2nd order in $q$ with $q$ being the filtration or Darcy velocity $q=\phi \velocity$, with $\perm$ $[m^2]$ being a pemeability tensor and the Forchheimer coefficient $\mathcal{I}_F$,
\begin{equation}
F = - \frac{\mu}{\perm} q - \frac{\mu}{\perm}\mathcal{I}_F \| q\| q .
\end{equation} 
Additionally the dynamic viscosity can be regarded as a function of concentration $\mu = \mu(\omega_C)$.
\subsection{Incompressible Porous Medium}
The porous medium is considered not to vary in space and time. Thus, all derivatives of $\phi$ vanish.
\subsection{Component Mass Balance Assumptions}
A scenario might be considered, where a certain fraction of component mass is stored within the solid matrix, incapable of moving with the fluid. If there is an equilibrium reaction between the amount of bound component mass with the amount of solved mass within an REV, their proportion is constant
\begin{align}
\frac{\omega_C^{\text{solid}}\rho_S}{\omega_C^{\text{fluid}}\rho_R} = K_d \phi = const,
\end{align}
with proportionality factor $K_d$.
Hence, by definition the amount of component bound to the solid matrix can only change by components getting solved within the fluid or vice versa. Thus, the total component mass change is reads
\begin{equation}
\frac{\partial \left(\omega_C^{\text{solid}} \rho_S +\phi\rho_R \omega_C^{\text{fluid}}  \right)}{\partial t} = \frac{\partial \left( \left(K_d \phi \rho_R +\phi\rho_R  \right)\omega_C^{\text{fluid}} \right)}{\partial t} .
\end{equation}
Since both, the solid matrix and the void space are assumed to be constant the expression can be further simplified as
\begin{equation}
\frac{\partial \left(K_d \phi \rho_R +\phi\rho_R  \right)\omega_C^{\text{fluid}} }{\partial t} = \left(K_d + 1 \right) \frac{\partial \phi \rho_R \omega_C}{\partial t}.
\end{equation}
For convenience $\omega_C^{\text{fluid}}=\omega_C$ is set and a retardation factor $R=1+K_d$ is introduced. In the following, there is no energetic cost for the transition of components from bound state to solved state taken into account.\\
Often, a process might be of interest, where the component concentration decays in time. This could be a consequence for radioactive decay. Hence, we add this decay term permanently to the equations. The widely used description of decay processes include such as sink terms $R_C$ and read
\begin{equation}
R_C = R \theta \phi \density_R \omega_C \label{equation_mass_decay}
\end{equation}
with $\theta$ $\left[\frac{1}{s}\right]$ denoting a decay rate. Note, both the bound and solved parts of the component mass are exposed to the decay process here.
\subsection{Momentum Balance Assumptions}
Within ground water flow models, innertia is generally neglected. Additionally, forces are only regarded in linear order of $q$ and no derivates of $q$ are considered. Taking those simplifications into account, the momentum balance Equation \eqref{equation_momentum_balance} simplifies to
\begin{equation}
\divergence \left( p \textbf{I} \right) = \left( \varrho_R g - \frac{\mu}{\perm} q \right)
\end{equation}
which can be used to calculate the filtration velocity
\begin{equation}
q =  -\frac{\perm}{\mu} \left(\nabla p- \rho_R g \right). \label{equation_darcy_law}
\end{equation}
\section{Complete Equations}
Here, all assumptions made in the previous chapter are taken into account. \\
Using Equation \eqref{equation_darcy_law} to rewrite \eqref{equation_mass_balance_total} the total mass balance reads
\begin{equation}
\phi\frac{\partial \rho_R}{\partial t} - \divergence \left(\frac{\perm}{\mu}\rho_R \left(\nabla p - \rho_R g \right)\right) + Q_p = 0 \quad 
\end{equation}
and can be rewritten using \eqref{equation_density_functional} 
\begin{equation}
\underbrace{\phi\frac{\partial  \rho_R}{\partial p}}_{\alpha_p}\frac{\partial p}{\partial t} + \underbrace{\phi\frac{\partial \rho_R}{\partial \omega_C}}_{\beta_p}\frac{\partial \omega_C}{\partial t} - \divergence \underbrace{\left(\frac{\perm}{\mu}\rho_R \left(\nabla p - \rho_R g \right)\right)}_{\gamma_p} + \underbrace{Q_p}_{\delta_p} = 0.\label{equation_mass_balance_total_used}
\end{equation}
Inserting \eqref{equation_darcy_law} and \eqref{equation_dispersive_flux} into the advective form for component mass balance Equation \eqref{equation_mass_balance_component_wise_advective} for the solved component mass and adding the decay term \eqref{equation_mass_decay} as well as the retardation factor introduced above leads to
\begin{equation}
\begin{split}
\underbrace{\phi \rho_R R}_{\beta_{\omega_C}} \frac{\partial \omega_C}{\partial t}   - \divergence \underbrace{\left( \rho_R \mathbf{D}_h \nabla \omega_C \right)}_{\gamma_{\omega_C}}+\underbrace{Q_{\omega_C}-\rho_R \left\langle\frac{\perm}{\mu} \left(\nabla p- \rho_R g \right) \bigg\lvert \nabla \omega_C \right\rangle - R \omega_C Q_p + R \theta \phi \density_R \omega_C}_{\delta_{\omega_C}} = 0. \label{equation_mass_balance_component_wise_used}
\end{split}
\end{equation}
The abbreviations $\alpha_i, \beta_i, \gamma_i, \delta_i$ are introduced to simplify calculations. The system can be solved by the parallel solution of Equation \eqref{equation_mass_balance_total_used} and Equation \eqref{equation_mass_balance_component_wise_used}.

\subsection{Boundary Conditions}
Dirichlet and Neumann conditions are defined at the boundary $\Gamma$ of $\Omega$. The corresponding areas, where the boundary conditions are defined are denoted with $\Gamma_D$ and $\Gamma_N$, resprectively, with $\Gamma = \Gamma_D \cup \Gamma_N $ and $\Gamma_D \cap \Gamma_N=\emptyset$.
\subsubsection{Pressure Boundary Conditions}
For the pressure Boundary conditions we can note, that flow boundary conditions are equivalent in it's form to Neumann type boundary conditions, hence the boundary conditions can be defined as
\begin{align}
 p - g_{D}^{p} &= 0 &\text{on } &\Gamma_D  &\text{(Dirichlet type boundary condition)},\\
\left\langle \underbrace{\frac{\perm}{\mu}\rho_R  \left(\nabla p - \rho_R g \right)}_{\gamma_p^N}\bigg\rvert n \right\rangle+ g_{N}^{p}  &= 0 & \text{on } & \Gamma_N  &\text{(Neumann type boundary condition).}
\end{align}
\subsection{Concentration Boundary Conditions}
For component concentration boundary conditions are implemented as
\begin{align}
 \omega_C - g_{D}^{\omega_C} &= 0 &\text{on } &\Gamma_D &\text{(Dirichlet type boundary condition)},\\
\left\langle\underbrace{\rho_R \mathbf{D}_h \nabla \omega_C}_{\gamma_{\omega_C}^N}\bigg\lvert n \right\rangle + g_{N}^{\omega_C}  &= 0 & \text{on } & \Gamma_N&\text{(Neumann type boundary condition)}.
\end{align}
%Flow boundary conditions for the concentration $\omega_C$, on a region of the boundaries $\Gamma_F \subset \Gamma_N$ can be defined as
%\begin{align}
%\left\langle\left(  \frac{\perm}{\mu} \rho_R \omega_C \left(\nabla p - \rho_R g \right) + \rho_R \mathbf{D}_h \nabla \omega_C\right)\bigg\lvert n \right\rangle + g_{F,C}  &= 0\\
%= -g_{N,C}-\omega_C g_{N,p} + g_{F,C}& =  0 & \text{on } & \Gamma_F&\text{(Flow type boundary condition)}.
%\end{align}
%Hence, the flow of concentration across the boundaries can be implemented by setting the value of $g_{N,C}$ according to
%\begin{align}
%g_{N,C} = g_{F,C} - \omega_C g_{N,p}.
%\end{align}
%Note, strictly spoken this corresponds to a Robin type boundary condition.