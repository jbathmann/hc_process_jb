\section{Weak Formulation and FEM Discretization for Groundwater Flow}

\subsection{Weak Formulation}
Multiplying \eqref{eq:groundwater_flow_equation} with $-1$ and summing up with \eqref{eq:NeumannTypeBC} leads to
\begin{equation} \label{eq:groundwater_flow_equation_weak_form_I}
- \poro \left(\pD{\density_R}{p} \pD{p}{t} + \pD{\density_R}{X} \pD{X}{t}
\right) +
\divergence \left[ \density_R \frac{\perm}{\viscosity} \grad \Psi \right]
+ Q_p + \scpr{\density_R \frac{\perm}{\viscosity} \grad \Psi}{\vn} + g_N = 0.
\end{equation}
Since \eqref{eq:groundwater_flow_equation_weak_form_I} holds true for arbitrary
points of the domain, the equation stays valid if it is multiplied by test
functions $\tvp, \bar \tvp \in H_0^1(\Omega)$ and the integration over
$\Omega$ and the Neumann boundary $\Gamma_{N,p}$, respectively:
\begin{equation} \label{weak_form_groundwater_flow_equation}
\begin{split}
0 &= \int_{\Omega} \tvp \left(- \poro \left(\pD{\density_R}{p} \pD{p}{t} +
	\pD{\density_R}{X} \pD{X}{t} \right)
	+ \divergence \left[ \density_R \frac{\perm}{\viscosity(T)} \grad \Psi
		\right] + Q_p \right) \d \vx\\
	&\qquad + \int_{\Gamma_N} \bar \tvp \left(\scpr{\density_R
		\frac{\perm}{\viscosity(T)} \grad \Psi}{\vn} + g_N \right) \d \sigma,
\end{split}
\end{equation}
or equivalently
\begin{equation} \label{weak_form_groundwater_flow_equation_II}
\begin{split}
0 &=- \int_{\Omega} \tvp \poro \left(\pD{\density_R}{p} \pD{p}{t} +
\pD{\density_R}{X} \pD{X}{t} \right) \d \vx +
\int_{\Omega} \tvp \divergence \left[ \density_R \frac{\perm}{\viscosity}
	\grad \Psi \right] \d \vx + \int_{\Omega} \tvp Q_p \d \vx\\
&\qquad + \int_{\Gamma_N} \bar \tvp \scpr{\density_R \frac{\perm}{\viscosity}
	\grad \Psi}{\vn} \d \sigma + \int_{\Gamma_N} \bar \tvp g_N \d \sigma.
\end{split}
\end{equation}
Integration by parts of the second term of
\eqref{weak_form_groundwater_flow_equation_II} results in
\begin{equation} \label{eq:integration_by_parts}
\int_{\Omega} \tvp \divergence \left[ \density_R \frac{\perm}{\viscosity} \grad \Psi
	\right]\d \vx =
- \int_{\Omega} \scpr{\grad \tvp}{\density_R \frac{\perm}{\viscosity} \grad \Psi}
	\d \vx +
\int_{\Omega} \divergence \left(\tvp \density_R \frac{\perm}{\viscosity} \grad \Psi \right) \d \vx
\end{equation}
Using Green's formula for the last term of the above expression
\begin{equation} \label{eq:greens_formulae}
\begin{split}
\int_{\Omega} \divergence \left(\tvp \density_R \frac{\perm}{\viscosity} \grad \Psi \right)\d \vx
&= \oint_{\Gamma} \scpr{\tvp \density_R \frac{\perm}{\viscosity} \grad \Psi}{\vn}\d \sigma
\\
&=  \int_{\Gamma_D} \scpr{\tvp \density_R \frac{\perm}{\viscosity} \grad \Psi}{\vn}\d \sigma
+ \int_{\Gamma_N} \scpr{\tvp \density_R \frac{\perm}{\viscosity} \grad \Psi}{\vn}\d \sigma
\end{split}
\end{equation}
and the integral on the Dirichlet boundary $\Gamma_D$ vanishes because $\tvp = 0$ holds. Finally, the expression \eqref{eq:integration_by_parts} takes the form
\begin{equation} \label{eq:weak_form_groundwater_flow_equation_III}
\int_{\Omega} \tvp \divergence \left[ \density_R \frac{\perm}{\viscosity} \grad \Psi \right]\d \vx
= - \int_{\Omega} \scpr{\grad \tvp}{\density_R \frac{\perm}{\viscosity} \grad \Psi} \d \vx \\
+ \int_{\Gamma_N} \scpr{\tvp \density_R \frac{\perm}{\viscosity} \grad \Psi}{\vn}\d \sigma
\end{equation}
Putting \eqref{eq:weak_form_groundwater_flow_equation_III} in
\eqref{weak_form_groundwater_flow_equation_II} yields to
\begin{align*}
0&= - \int_{\Omega} \tvp \poro \left(\pD{\density_R}{p} \pD{p}{t}
		+ \pD{\density_R}{X} \pD{X}{t} \right) \d \vx \\
	&\qquad - \int_{\Omega} \scpr{\grad \tvp}{\density_R
			\frac{\perm}{\viscosity} \grad \Psi} \d \vx
	+ \int_{\Gamma_N} \scpr{\tvp \density_R \frac{\perm}{\viscosity}
			\grad \Psi}{\vn}\d \sigma \\
	&\qquad + \int_{\Omega} \tvp Q_p \d \vx
	+ \int_{\Gamma_N} \bar \tvp \scpr{\density_R \frac{\perm}{\viscosity}
		\grad \Psi}{\vn} \d \sigma
	+ \int_{\Gamma_N} \bar \tvp g_N \d \sigma
\end{align*}
Since the test functions are arbitrary, by setting $\tvp = - \bar \tvp$ the second and
fourth term cancel each other. Multiplying by $-1$ results in
\begin{equation} \label{eq:weak_form_groundwater_flow_equation_IV}
0 = - \int_{\Omega} \tvp \poro \pD{\density_R}{p} \pD{p}{t} \d \vx
	- \int_{\Omega} \tvp \poro \pD{\density_R}{X} \pD{X}{t} \d \vx
	+ \int_{\Omega} \scpr{\grad v}{\density_R \frac{\perm}{\viscosity}
		\grad \Psi} \d \vx
	- \int_{\Omega} \tvp Q_p \d \vx
	- \int_{\Gamma_N} \tvp g_N \d \sigma.
\end{equation}

\section{Finite Element Discretization}

\begin{equation} \label{eq:fem_approximation}
p \approx \sum N_j^p a_j^p = \vN^p \va^p,
\quad \Psi \approx \sum N_j^p a_j^p + \density_R \, g \, z
\end{equation}
where $N_j^p(x,y,z)$ are the \emph{shape functions} and $a_j^p(t)$ are
coefficients. The case where the test functions are approximated by the same
shape function, i.e.
\begin{equation} \label{eq:galerkin_principle}
\tvp = N_i^p,
\end{equation}
is denoted as Galerkin principle.

Substituting \eqref{eq:fem_approximation} and \eqref{eq:galerkin_principle} in
\eqref{eq:weak_form_groundwater_flow_equation_IV} leads to
\begin{equation} \label{eq:fem_groundwater_flow_equation}
\begin{split}
0 &=
\left[\int_{\Omega} N_i^p \poro \pD{\density_R}{p} N_j^p \d \vx \right]
		\pD{\va_{j}^p}{t} +
	\left[\int_{\Omega} N_i^p \poro \pD{\density_R}{X} N_j^X \d \vx \right]
		\pD{\va_j^X}{t} +
	\left[ \int_{\Omega} \nabla^T N_i^p \density_R \frac{\perm}{\viscosity}
		\nabla \vN^p \d \vx \right] \va^p \\
&\qquad +
	\int_{\Omega} \nabla^T N_i^p \density_R \frac{\perm \, \density_R \,
		g}{\viscosity}
\ve_{z} \d \vx
- \int_{\Omega} N_i Q_p \d \vx
- \int_{\Gamma_N} N_i g_N \d \sigma,
\end{split}
\end{equation}
$i,j = 1, \dotsc, n,$ which is a set of linear equations of the form
\begin{equation}
\mC \dot \va + \mK \va + \vf = 0
\end{equation}
with
\begin{align}
\mC_{pp}^{ij} &= \int_{\Omega} N_i^p \poro \pD{\density_R}{p} \vN_j^p \d \vx\\
\mC_{pX}^{ij} &= \int_{\Omega} N_i^p \poro \pD{\density_R}{X} \vN_j^X \d \vx\\
\mK_{pp}^{ij} &= \int_{\Omega} \nabla^T N_i^p \density_R \frac{\perm}{\viscosity}
	\nabla \vN_j^p \d \vx \\
\label{eq:rhs}
\vf_p^i &= - \int_{\Omega} N_i^p Q\d \vx
- \int_{\Gamma_N} N_i^p g_N\d \sigma
+ \int_{\Omega} \nabla^T N_i^p \density_R \frac{\perm \, \density_R \, g}{\viscosity}
\ve_{z} \d \vx
\end{align}