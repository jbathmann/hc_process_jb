\chapter{Component Transport Process}
Let $V$ be a representable volume and $V_\mathrm{void}$ the pore or void volume
of the porous medium. The quotient of mass $m$ and $V$ defines the apparent
density $\density = \frac{m}{V}.$ The real density $\density_R =
\frac{m}{V_\mathrm{void}}$ is related to the apparent density by the porosity
\begin{equation} \label{eq:defi_poro}
\poro = \frac{V_{\mathrm{void}}}{V}
\end{equation}
which yields to $\density = \poro \density_R.$

Let $n$ be the amount of a substance. The apparent concentration $C$ of a solute
in $V$ is $C = \frac{n}{V}.$ The real concentration $C_R =
\frac{n}{V_\mathrm{void}}$ is linked to the apparent concentration via
\eqref{eq:defi_poro}: $C = \poro C_R.$

The mass $m$ is the amount of the substance times the molar mass $M$ of the
substance: $m = M \cdot n.$ The apparent density and real density are
proportional to the apparent concentration and real concentration:
\begin{equation}
\density = \frac{M \cdot n}{V} = M C \quad \textrm{and} \quad
\density_R = \frac{M \cdot n}{V_\textrm{void}} = M C_R.
\end{equation}

\cite{KNABNER2001319}
\cite{KOLDITZ199827}
\cite{DIERSCH1998401}
\cite{Diersch2002899}

\section{Constitutive Law}
Analogously to \eqref{eq:balance_relation}, the fundamental of the component
transport is the continuity equation
\begin{equation}
\pD{(\poro C_R)}{t} + \divergence \vJ - Q_C = 0.
\end{equation}
Depending on the constitutive law that describes the flow $\vJ$, we obtain the
balance equation of the considered process. Important practical laws are
\begin{equation} \label{eq:diffusion_law}
	\vJ^{(1)} = - \dispersion \grad \left(\poro C_R\right)
	= - \dispersion \nabla \left(\poro C_R\right),
\end{equation}
where $\dispersion$ is the hydrodynamic dispersion tensor.
\eqref{eq:diffusion_law} describes diffusive flow and
\begin{equation} \label{eq:advection}
	\vJ^{(2)} = \vv \poro C_R = \vq C_R \quad
		\text{(where $q$ is a Darcy velocity vector)}
\end{equation}
describes advective flow. It is also possible to combine
\eqref{eq:diffusion_law} and \eqref{eq:advection}. The description of the flow
by a combination of \eqref{eq:diffusion_law} and \eqref{eq:advection} yields to
\begin{equation} \label{eq:diffusion_advection}
	\pD{(\poro C_R)}{t} - \nabla \cdot \left[\dispersion \nabla \poro C_R\right]
	+ \nabla \left(\vq C_R\right) - Q_C = 0.
\end{equation}
\marginpar{assumption: incompressibility already included}
The advective part $\vq C_R$ is driven by the Darcy velocity $\vq$ of the
coupled groundwater flow process.

todo: check the following and discuss with JB, DN and NG

For the process at hand the density is substituted by $R C,$ where $R$ denotes
the retardation factor. Finally, the term
\begin{equation}
	R \vartheta \poro C
\end{equation}
describing the decay of the chemical species is integrated into the equation
which acts similarly to a sink term. Here $\vartheta$ is the decay rate. The
final mass diffusion equation reads:
\begin{equation} \label{eq:HC_equation_general}
\pD{}{t} \left(\poro R C \right)
+ \divergence \left(\vq C - \dispersion \grad C \right)
+ \poro R \vartheta C - Q_C = 0.
\end{equation}
The hydrodynamic dispersion tensor is
\begin{equation*}
\dispersion = \left(\poro D_d + \beta_T \vnorm{\vq} \right) \mI +
\left(\beta_L - \beta_T\right) \frac{\vq \vq^T}{\vnorm{\vq}},
\end{equation*}
where
\begin{itemize}
	\item $\beta_L$ is the longitudinal dispersivity of chemical species,
	\item $\beta_T$ is the transverse dispersivity of chemical species,
	\item $D_d$ is the molecular diffusion coefficient.
\end{itemize}
Porosity variations and solid motions are neglected, i.e.,
$\displaystyle{\pD{\poro}{t} = 0}.$ Furthermore, it is assumed that the
retardation factor is not time dependent:
\begin{equation} \label{eq:HC_equation}
	\poro R \pD{C}{t}
	+ \divergence \left(\vq C - \dispersion \grad C \right)
	+ \poro R \vartheta C - Q_C = 0
\end{equation}

\section{Boundary Conditions}
\begin{align}
C - g_D^C &= 0 \quad \textnormal{on} \quad \Gamma_D
\quad \textnormal{(Dirichlet type boundary conditions)}\\
\scpr{\mD \grad C}{\vn} + g_N^C &= 0\quad \textnormal{on} \quad \Gamma_N
\quad \textnormal{(Neumann type boundary conditions)}
\label{eq:NeumannTypeBC_C}
\end{align}

\section{Weak Formulation}
The integration of the Neumann type boundary condition \eqref{eq:NeumannTypeBC_C}
into \eqref{eq:HC_equation}, multiplying with
arbitrary test functions $v, \bar v \in H_0^{1}(\Omega)$ and integration over
$\Omega$ results in
\begin{equation} \label{eq:HC_weak_formulation}
\begin{split}
0 &=\int_{\Omega} v\cdot \poro \cdot R \cdot \pD{C}{t} \d \Omega
+ \int_{\Omega} v\cdot \divergence \left(\vq C - \mD \grad C \right) \d \Omega
\\
&\quad
+ \int_{\Omega} v\cdot \left[\vartheta \cdot \poro \cdot R \cdot C\right] \d \Omega
- \int_{\Omega} v\cdot Q_C \d \Omega
+ \int_{\Gamma_N} \bar v\cdot\left[\scpr{\mD\grad C}{\vn} + g_N^C\right]
\d \sigma
\end{split}
\end{equation}
Integration by parts of the second term in the above equation yields:
\begin{equation} \label{eq:weak_formulation_heat_conduction_equation_I}
\int_{\Omega} v \cdot \divergence \left(\vq C - \mD \grad C \right) \d \Omega
= - \int_{\Omega} \scpr{\grad v}{\vq C - \mD \grad C} \d \Omega
+ \int_{\Omega} \divergence \left[v \left(\vq C - \mD \grad C\right) \right] \d \Omega
\end{equation}
Using Green's formula for the last term of the above expression
\begin{align*}
\int_{\Omega} \divergence \left[v \left(\vq C - \mD \grad C\right) \right] \d \Omega
&= \oint_{\Gamma} \scpr{v \left(\vq C - \mD \grad C\right)}{\vn}\d \sigma\\
&= \int_{\Gamma_D} \scpr{v \left(\vq C - \mD \grad C\right)}{\vn}\d \sigma
+ \int_{\Gamma_N} \scpr{v \left(\vq C - \mD \grad C\right)}{\vn}\d \sigma
\end{align*}
and since $v$ vanishes on $\Gamma_D$ the integral over $\Gamma_D$ also vanishes,
this leads to
\begin{equation} \label{eq:greens_formulae_II_result}
\int_{\Omega} v \cdot \divergence \left(\vq C - \mD \grad C \right) \d \Omega
= - \int_{\Omega} \scpr{\grad v}{\vq C - \mD \grad C} \d \Omega
+ \int_{\Gamma_N} \scpr{v \left(\vq C - \mD \grad C\right)}{\vn}\d \sigma
\end{equation}
Thus \eqref{eq:HC_weak_formulation} reads:
\begin{equation} \label{eq:HC_weak_formulation_II}
\begin{split}
0 &= \int_{\Omega} v\cdot \poro \cdot R \cdot \pD{C}{t} \d \Omega
- \int_{\Omega} \scpr{\grad v}{\vq C - \mD \grad C} \d \Omega
+ \int_{\Gamma_N} \scpr{v \left(\vq C - \mD \grad C\right)}{\vn}\d \sigma\\
&\qquad
+ \int_{\Omega} v\cdot \left[\vartheta \cdot \poro \cdot R \cdot C\right] \d \Omega
- \int_{\Omega} v\cdot Q_C \d \Omega
+ \int_{\Gamma_N} \bar v\cdot\left[\scpr{\mD\grad C}{\vn} + g_N^C\right]
\d \sigma
\end{split}
\end{equation}
Setting $v = \bar v:$
\begin{equation} \label{eq:HC_weak_formulation_III}
\begin{split}
0 &= \int_{\Omega} v\cdot \poro \cdot R \cdot \pD{C}{t} \d \Omega
- \int_{\Omega} \scpr{\grad v}{\vq C - \mD \grad C} \d \Omega
+ \int_{\Gamma_N} \scpr{v \vq C}{\vn} \d \sigma
\\
&\qquad
+ \int_{\Omega} v\cdot \left[\vartheta \cdot \poro \cdot R \cdot C\right] \d \Omega
- \int_{\Omega} v\cdot Q_C \d \Omega
+ \int_{\Gamma_N} v\cdot g_N^C \d \sigma
\end{split}
\end{equation}

\section{Finite Element Discretization}
The concentration is approximated by:
\begin{equation} \label{eq:fem_approximation_concentration}
C \approx \sum N_j^C a_{j}^C = \vN^C \va^c
\end{equation}
using the shape functions $N_j^C(\vx)$ and time dependent coefficients $a_{j}^p(t)$.
Using the shape functions again as test functions (Galerkin principle) the
discretization of \eqref{eq:HC_weak_formulation_III}) takes the following form
\begin{equation} \label{eq:HC_fem}
\begin{split}
0 &= \int_{\Omega} N_i^C \cdot \poro \cdot R \cdot N_j^C \pD{a_j^C}{t} \d \Omega
- \int_{\Omega} \nabla^T N_i^C \cdot \vq \cdot N_j^C a_{j}^C \d \Omega
+ \int_{\Omega} \nabla^T N_i^C \mD \nabla N_j^C a_{j}^C \d \Omega\\
&\qquad
+ \int_{\Gamma_N} \left(N_i^C \vq^T N_j^C a_{j}^C\right) \vn \, \d \sigma\\
&\qquad
	+ \int_{\Omega} N_i^C\cdot \left[\vartheta \cdot \poro \cdot R \cdot N_j^C a_{j}^C\right] \d \Omega
	- \int_{\Omega} N_i^C\cdot Q_C \d \Omega
	+ \int_{\Gamma_N} N_i^C\cdot g_N^C \d \sigma
\end{split}
\end{equation}
In \eqref{eq:HC_fem} the Darcy velocity $\vq$ is assumed
to be known from the hydrological process. In contrast to this approach
pressure $p$ in the Darcy velocity can be expressed as an approximation in
terms of shape functions $\vN_i^p$
\begin{equation}
\vq \approx \frac{\kappa}{\mu} \left(\nabla \vN^p \va^p + \density_R \cdot g \cdot
\ve_{z}\right).
\end{equation}
\marginpar{is this correct?}

\begin{equation} \label{eq:HC_fem_full}
\begin{split}
0 &= \int_{\Omega} N_i^C \cdot \poro \cdot R \cdot N_j^C \pD{a_{j}^C}{t} \d \Omega
- \int_{\Omega} \nabla^T N_i^C \cdot
	\frac{\kappa}{\mu} \left(\nabla \vN^p \va^p + \density_R \cdot g \cdot
		\ve_{z}\right)
	\cdot N_j^C a_{j}^C \d \Omega\\
	&\qquad
+ \int_{\Omega} \nabla^T N_i^C \mD \nabla N_j^C a_{j}^C \d \Omega
+ \int_{\Gamma_N} \left(N_i^C
	\frac{\kappa}{\mu} \left(\nabla \vN^p \va^p + \density_R \cdot g \cdot
	\ve_{z}\right)^T
	N_j^C a_{j}^C\right) \vn \, \d \sigma\\
&\qquad
+ \int_{\Omega} N_i^C\cdot \left[\vartheta \cdot \poro \cdot R \cdot N_j^C a_{j}^C\right] \d \Omega
- \int_{\Omega} N_i^C\cdot Q_C \d \Omega
+ \int_{\Gamma_N} N_i^C\cdot g_N^C \d \sigma
\end{split}
\end{equation}
This is a set of equations of the form
\begin{equation}
\mC \dot \va + \mK \va + \vf = 0
\end{equation}
with
\begin{align}
\label{eq:HC_K_BoussinesqApproximation}
\begin{split}
\mK_{CC}^{ij} &= - \int_{\Omega} \nabla^T N_i^C \cdot \vq \cdot N_j^C \d \Omega
+ \int_{\Omega} \nabla^T N_i^C \mD \nabla \vN_j^C \d \Omega
+ \int_{\Gamma_N} \left(N_i^C \cdot \vq^T N_j^C\right)^T \vn \d \sigma\\
&\qquad + \int_{\Omega} N_i^C\cdot \left[\vartheta \cdot \poro \cdot R \cdot N_j^C\right] \d \Omega,
\end{split}
\\
\label{eq:rhs_T}
\vf_C^{i} &= - \int_{\Omega} N_i^C Q_C\d \Omega
	+ \int_{\Gamma_N} N_i^C g_N^C\d\sigma,\\
\mC_{CC}^{ij} &= \int_{\Omega} \vN_i^C \cdot \poro \cdot R \cdot \vN_j^C \d \Omega.
\end{align}
In \eqref{eq:HC_K_BoussinesqApproximation} the Darcy velocity $\vq$ is assumed
to be known from the hydrological process. In contrast to this approach
pressure $p$ in the Darcy velocity can be expressed as an approximation by
shape functions $\vN_i^p$
\begin{equation}
\vq = \frac{\kappa}{\mu} \grad \left(p + \varrho \cdot g \cdot z\right)
\approx \frac{\kappa}{\mu} \left(\nabla N_i^p + \varrho \cdot g \cdot
\ve_{z}\right).
\end{equation}
Thus, some terms of $\mK_{ij}^{CC}$ are moved to the coupling matrix:
\begin{equation}
\mK_{Cp}^{ij} = - \int_{\Omega} \nabla^T N_i^C \cdot \frac{\kappa}{\mu}
\left(\nabla N_i^p + \varrho \cdot g \cdot \ve_{z}\right) \cdot N_j^C \d \Omega
\end{equation}

\section{Evaluating Dominance of Effects} \marginpar{see 7.7 in \cite{JacobBear2010}}
Substitute variables and coefficients that appear in \eqref{eq:HC_equation}:
\begin{equation} \label{eq:HC_equation_dimension_analysis_storage}
\poro R \pD{C}{t} = \poro^\ast \poro_c R^\ast R_c \left(\pD{C}{t}\right)^\ast \left(\pD{C}{t}\right)_c
	= \poro^\ast \poro_c R^\ast R_c \left(\pD{C}{t}\right)^\ast \frac{(\Delta C)_c}{(\Delta t)_c}
	= \poro^\ast R^\ast \left(\pD{C}{t}\right)^\ast \poro_c R_c \frac{(\Delta C)_c}{t_c}
\end{equation}
where $t_c = (\Delta t)_c.$
\begin{equation} \label{eq:HC_equation_dimension_analysis_advection}
\begin{split}
\divergence \left(\vq C \right)
	&= \pD{\vq C}{x_i}
	= \pD{\vq}{x_i} C + \pD{C}{x_i} \vq
	= \left(\pD{\vq}{x_i}\right)^\ast \frac{(\Delta \vq)_c}{L_c^{(\vq)}} C^\ast C_c +
	\left(\pD{C}{x_i}\right)^\ast \frac{(\Delta C)_c}{L_c^{(C)}} \vq^\ast \vq_c\\
	&= \pD{\vq^\ast}{x_i^\ast} C^\ast
		\frac{(\Delta \vq)_c}{L_c^{(\vq)}}  C_c +
	\vq^\ast \pD{C^\ast}{x_i^\ast} \frac{\vq_c (\Delta C)_c}{L_c^{(C)}}
\end{split}
\end{equation}
\begin{equation} \label{eq:HC_equation_dimension_analysis_diffusion}
\begin{split}
\divergence \left(\dispersion \grad C \right)
	&= \pD{}{x_i} \left( \mD \pD{C}{x_i} \right)
	= \left(\pD{}{x_i}\right)^\ast \frac{1}{L_c^{(C)}} \left( \mD^\ast
	D_c \left(\pD{C}{x_i}\right)^\ast \frac{(\Delta C)_c}{L_c^{(C)}} \right)\\
	&= \pD{}{x_i^\ast} \left( \mD^\ast \pD{C^\ast}{x_i^\ast}\right)
	\frac{D_c (\Delta C)_c}{{L_c^{(C)}}^2}
\end{split}
\end{equation}
\begin{equation} \label{eq:HC_equation_dimension_analysis_decay}
\poro R \vartheta C
	= \poro^\ast \poro_c R^\ast R_c \vartheta^\ast \vartheta_c C^\ast C_c
	= \poro^\ast R^\ast \vartheta^\ast C^\ast \poro_c R_c \vartheta_c C_c
\end{equation}
With $L_c^{(C)} = L_c^{(q)} = L_c$
\begin{equation} \label{eq:HC_equation_dimension_analysis_complete}
\begin{split}
0 &= \poro^\ast R^\ast \left(\pD{C}{t}\right)^\ast \poro_c R_c \frac{(\Delta C)_c}{t_c}
+ \frac{(\Delta \vq)_c}{L_c}  C_c \pD{\vq^\ast}{x_i^\ast} C^\ast
+ \frac{\vq_c (\Delta C)_c}{L_c} \vq^\ast \pD{C^\ast}{x_i^\ast}
- \frac{D_c (\Delta C)_c}{L_c^2}
	\pD{}{x_i^\ast} \left( \mD^\ast \pD{C^\ast}{x_i^\ast}\right) \\
&\quad + \poro_c R_c \vartheta_c C_c \poro^\ast R^\ast \vartheta^\ast C^\ast
\end{split}
\end{equation}

\begin{equation} \label{eq:HC_equation_dimension_analysis_complete_simplified}
\begin{split}
0 &= \poro^\ast R^\ast \left(\pD{C}{t}\right)^\ast
+ \frac{1}{\poro_c R_c} \left(
	\frac{(\Delta \vq)_c}{(\Delta C)_c} C_c
	\frac{t_c}{L_c} \pD{\vq^\ast}{x_i^\ast} C^\ast
+ \vq_c \frac{t_c}{L_c} \vq^\ast \pD{C^\ast}{x_i^\ast}
- D_c \frac{t_c}{L_c^2}
\pD{}{x_i^\ast} \left( \mD^\ast \pD{C^\ast}{x_i^\ast}\right) \right) \\
&\quad + \frac{\vartheta_c C_c t_c}{(\Delta C)_c} \poro^\ast R^\ast \vartheta^\ast C^\ast
\end{split}
\end{equation}
\begin{equation} \label{eq:HC_equation_dimension_analysis_complete_simplified_I}
\begin{split}
0 &= \poro^\ast R^\ast \left(\pD{C}{t}\right)^\ast
+ \frac{D_c t_c}{\poro_c R_c L_c^2} \left(
\frac{(\Delta \vq)_c}{(\Delta C)_c} C_c
\frac{L_c}{D_c} \pD{\vq^\ast}{x_i^\ast} C^\ast
+ \vq_c \frac{L_c}{D_c} \vq^\ast \pD{C^\ast}{x_i^\ast}
- \pD{}{x_i^\ast} \left( \mD^\ast \pD{C^\ast}{x_i^\ast}\right) \right) \\
&\quad + \frac{\vartheta_c C_c t_c}{(\Delta C)_c} \poro^\ast R^\ast \vartheta^\ast C^\ast
\end{split}
\end{equation}
With $C_c = (\Delta C)_c$ and $\vq_c = (\Delta \vq)_c$
\begin{equation} \label{eq:HC_equation_dimension_analysis_complete_simplified_II}
\begin{split}
0 &= \poro^\ast R^\ast \left(\pD{C}{t}\right)^\ast
+ \frac{D_c t_c}{\poro_c R_c L_c^2} \left(
\vq_c \frac{L_c}{D_c} \pD{\vq^\ast}{x_i^\ast} C^\ast
+ \vq_c \frac{L_c}{D_c} \vq^\ast \pD{C^\ast}{x_i^\ast}
- \pD{}{x_i^\ast} \left( \mD^\ast \pD{C^\ast}{x_i^\ast}\right) \right) \\
&\quad + \vartheta_c t_c \poro^\ast R^\ast \vartheta^\ast C^\ast
\end{split}
\end{equation}
\begin{equation} \label{eq:HC_equation_dimension_analysis_complete_simplified_III}
0 = \poro^\ast R^\ast \left(\pD{C}{t}\right)^\ast
+ \frac{D_c t_c}{\poro_c R_c L_c^2} \left(
\vq_c \frac{L_c}{D_c} \pD{\vq^\ast C^\ast}{x_i^\ast}
- \pD{}{x_i^\ast} \left( \mD^\ast \pD{C^\ast}{x_i^\ast}\right) \right)
+ \vartheta_c t_c \poro^\ast R^\ast \vartheta^\ast C^\ast
\end{equation}
Setting $\Peclet = \displaystyle{\vq_c \frac{L_c}{D_c}}$, the Peclet number and
$\Fourier = \displaystyle{\vq_c \frac{t_c D_c}{L_c^2}}$ the Fourier number
\begin{equation} \label{eq:HC_equation_dimension_analysis_complete_simplified_IV}
0 = \poro^\ast R^\ast \left(\pD{C}{t}\right)^\ast
+ \frac{\Fourier}{\poro_c R_c} \pD{}{x_i^\ast} \left(
\Peclet \cdot \vq^\ast C^\ast - \left( \mD^\ast \pD{C^\ast}{x_i^\ast}\right) \right)
+ \vartheta_c t_c \poro^\ast R^\ast \vartheta^\ast C^\ast
\end{equation}

\section{Stability Criteria}
\begin{equation}
\Delta t < \frac{h^2}{a}
\end{equation}
Unsure:
\begin{equation}
a =\frac{\dispersion}{\poro R}
\end{equation}


